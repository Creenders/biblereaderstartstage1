package bibleReader.model;

/**
 * A simple class that stores the book, chapter number, and verse number.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Collin Reenders (provided the implementation), 2020
 */
public class Reference implements Comparable<Reference> {

	// Added the appropriate fields
	private BookOfBible book;
	private int chapter;
	private int verse;

//____________________________________________________________________________________________________________________________

	// Implemented the methods.

	public Reference(BookOfBible book, int chapter, int verse) {
		// Implemented Constructor, test passed (getBook passed therefore constructor
		// works).
		this.book = book;
		this.chapter = chapter;
		this.verse = verse;
	}

	public String getBook() {
		// Implemented getBook(), test passed.
		return book.toString();
	}

	public BookOfBible getBookOfBible() {
		// Implemented getBookOfBible(), test passed.
		return book;
	}

	public int getChapter() {
		// Implemented getChapter(), test passed.
		return chapter;
	}

	public int getVerse() {
		// Implemented getVerse(), test passed.
		return verse;
	}

	@Override
	public String toString() {
		// Implemented toString(), test passed.
		return book.toString() + " " + Integer.toString(chapter) + ":" + Integer.toString(verse);
	}

	@Override
	public boolean equals(Object other) {
		// Implemented equals, tests passed.

		Reference r;
		if (other instanceof Reference) {
			r = (Reference) other;

			// check for equality using hashcode.
			if (this.hashCode() == r.hashCode()) {
				return true;
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		return toString().hashCode();
	}

	@Override
	public int compareTo(Reference otherRef) {
		// Implemented.

		// Checking degree of likeness to compare between References
		if (book.compareTo(otherRef.getBookOfBible()) == 0) {
			if (chapter == otherRef.getChapter()) {
				if (verse == otherRef.getVerse()) {
					return 0;
				}
				return (verse - otherRef.getVerse());
			}
			return (chapter - otherRef.getChapter());
		}
		return book.compareTo(otherRef.getBookOfBible());
	}
}
