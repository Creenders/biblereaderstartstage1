package bibleReader.model;

/**
 * A class which stores a verse.
 * 
 * @author Charles Cusack (Skeleton Code), 2008, edited 2012.
 * @author Collin Reenders (provided the implementation), 2020
 */
public class Verse implements Comparable<Verse> {

	// Added Fields here. 
	private Reference ref;
	private String text;
//____________________________________________________________________________________________________________________________

	//Implemented necessary methods.
	
	public Verse(Reference r, String t) {
		// Implemented Constructor taking two parameters.
		this.ref = r;
		this.text = t;
	}

	public Verse(BookOfBible book, int chapter, int verse, String text) {
		// Implemented Constructor taking 4 parameters.
		this.ref = new Reference(book, chapter, verse);
		this.text = text;
	}

	/**
	 * Returns the Reference object for this Verse.
	 * 
	 * @return A reference to the Reference for this Verse.
	 */
	public Reference getReference() {
		// Implemented
		return ref;
	}

	/**
	 * Returns the text of this Verse.
	 * 
	 * @return A String representation of the text of the verse.
	 */
	public String getText() {
		// Implemented
		return text;
	}

	/**
	 * Returns a String representation of this Verse, which is a String
	 * representation of the Reference followed by the String representation of the
	 * text of the verse.
	 */
	@Override
	public String toString() {
		// TODO Implement me: Stage 2
		return ref.toString() + " " + text;
	}

	/**
	 * Should return true if and only if they have the same reference and text.
	 */
	@Override
	public boolean equals(Object other) {
		// Implemented

		Verse v;
		if (other instanceof Verse) {
			v = (Verse) other;

			// check for equality using hashcode
			if (ref.compareTo(((Verse) other).getReference()) == 0) {
				if (text.compareTo(((Verse) other).getText()) == 0) {
					return true;
				}
			}
		}
		return false;
	}

	@Override
	public int hashCode() {
		// TODO Implement me: Stage 2
		// Don't forget the rules for how equals and hashCode are supposed to be
		// related.
		return -1;
	}

	/**
	 * From the Comparable interface. See the API for how this is supposed to work.
	 */
	@Override
	public int compareTo(Verse other) {
		// TODO Implement me: Stage 2

		if (ref.compareTo(other.getReference()) == 0) {
			if (text.compareTo(other.getText()) == 0) {
				return 0;
			}
			return (text.compareTo(other.getText()));
		}
		return (ref.compareTo(other.getReference()));
	}

	/**
	 * Return true if and only if this verse the other verse have the same
	 * reference. (So the text is ignored).
	 * 
	 * @param other the other Verse.
	 * @return true if and only if this verse and the other have the same reference.
	 */
	public boolean sameReference(Verse other) {
		return getReference().equals(other.getReference());
	}

}
